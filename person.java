package supershop;

import java.util.Scanner;

public class Person {
   public String id;
   public String name;
   Scanner input = new Scanner(System.in);
   public void setPerson(){
       System.out.println("Input person id:");
       id = input.nextLine();
       
       System.out.println("Input person name:");
       name = input.nextLine();
   }
   
   public void printPerson(){
       System.out.println("id = " + id);
       System.out.println("name = " + name);
   }
}
